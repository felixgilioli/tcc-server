package br.com.felix.tccserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
class TccServerApplication

fun main(args: Array<String>) {
	runApplication<TccServerApplication>(*args)
}
