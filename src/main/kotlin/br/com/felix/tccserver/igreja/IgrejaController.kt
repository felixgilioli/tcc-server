package br.com.felix.tccserver.igreja

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("igreja")
class IgrejaController(private val service: IgrejaService) : CrudController<Igreja, Long>() {

    override fun getService() = service

    @GetMapping("complete")
    fun complete(@RequestParam("query") query: String) = service.complete(query)
}
