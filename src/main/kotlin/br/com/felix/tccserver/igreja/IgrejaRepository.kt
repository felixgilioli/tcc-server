package br.com.felix.tccserver.igreja

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface IgrejaRepository : JpaRepository<Igreja, Long> {

    @Query("select i from Igreja i where lower(i.razaoSocial) like %:query% or lower(i.nomeFantasia) like %:query% ")
    fun complete(query: String): List<Igreja>?

}
