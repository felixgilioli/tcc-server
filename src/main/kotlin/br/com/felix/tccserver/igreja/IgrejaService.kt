package br.com.felix.tccserver.igreja

import br.com.felix.tccserver.framework.crud.CrudService

interface IgrejaService : CrudService<Igreja, Long> {

    fun complete(query: String): List<Igreja>
}
