package br.com.felix.tccserver.igreja

import javax.persistence.*

@Entity
data class Igreja(

        @Column(length = 100, nullable = false)
        var razaoSocial: String,

        @Column(length = 100, nullable = false)
        var nomeFantasia: String,

        @Column(length = 20, nullable = false)
        var cnpj: String,

        @Column(length = 100, nullable = false)
        var endereco: String,

        @ManyToOne
        @JoinColumn
        var matriz: Igreja? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
