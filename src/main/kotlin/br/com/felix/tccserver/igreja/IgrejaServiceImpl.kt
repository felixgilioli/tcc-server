package br.com.felix.tccserver.igreja

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class IgrejaServiceImpl(private val repository: IgrejaRepository) : IgrejaService, CrudServiceImpl<Igreja, Long>() {

    override fun getRepository() = repository

    @Transactional(readOnly = true)
    override fun complete(query: String) = repository
            .complete(query.toLowerCase()) ?: emptyList()
}
