package br.com.felix.tccserver.membro

import br.com.felix.tccserver.framework.crud.CrudService
import java.time.LocalDate

interface MembroService : CrudService<Membro, Long> {

    fun findAniversariantes(dataAniversario: LocalDate): List<Membro>

    fun findByIgreja(igreja: Long): List<Membro>

    fun complete(query: String): List<Membro>

    fun membrosPorSexo(igreja: Long?): Any

    fun membrosPorFaixaEtaria(igreja: Long?): Any

    fun membrosAtivosEInativos(igreja: Long?): Any
}
