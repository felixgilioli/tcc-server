package br.com.felix.tccserver.membro

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.framework.email.EmailModel
import br.com.felix.tccserver.framework.email.EmailSender
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Service
class MembroServiceImpl(private val repository: MembroRepository,
                        private val emailSender: EmailSender) : MembroService, CrudServiceImpl<Membro, Long>() {

    override fun getRepository() = repository

    @Scheduled(cron = "0 42 19 * * *")
    fun sendEmailAniversariantes() {
        val aniversariantes = this.findAniversariantes(LocalDate.now())

        val emails = aniversariantes
                .mapNotNull { m -> m.email }
                .toSet()

        emailSender.send(EmailModel(
                emails,
                "Feliz Aniversário",
                "Parabéns pelo seu aniversário, desejamos muitas felicidades!"
        ))
    }

    @Transactional(readOnly = true)
    override fun findAniversariantes(dataAniversario: LocalDate) =
            repository.findByDataNascimento(dataAniversario) ?: emptyList()

    @Transactional(readOnly = true)
    override fun findByIgreja(igreja: Long) =
            repository.findByIgrejaId(igreja) ?: emptyList()

    @Transactional(readOnly = true)
    override fun complete(query: String) = repository
            .findByNomeContainingIgnoreCase(query) ?: emptyList()

    override fun membrosPorSexo(igreja: Long?): Any {
        val membros = if (igreja != null) repository.findByIgrejaId(igreja) else repository.findAll()

        if (membros.isNullOrEmpty()) throw IllegalArgumentException()

        val membrosPorSexo = membros.groupBy(Membro::sexo)

        val totalM = if (membrosPorSexo[Sexo.M].isNullOrEmpty()) 0 else membrosPorSexo.getValue(Sexo.M).count()
        val totalF = if (membrosPorSexo[Sexo.F].isNullOrEmpty()) 0 else membrosPorSexo.getValue(Sexo.F).count()

        val data: MutableMap<String, Any> = mutableMapOf()

        val dataSet: MutableList<MutableMap<String, Any>> = mutableListOf()
        dataSet.add(hashMapOf("label" to "Masculino", "data" to listOf(totalM), "backgroundColor" to "#0D47A1"))
        dataSet.add(hashMapOf("label" to "Feminino", "data" to listOf(totalF), "backgroundColor" to "#6200EA"))
        data["datasets"] = dataSet

        return data
    }

    override fun membrosPorFaixaEtaria(igreja: Long?): Any {
        val membros = if (igreja != null) repository.findByIgrejaId(igreja) else repository.findAll()

        if (membros.isNullOrEmpty()) throw IllegalArgumentException()

        val bebe = membros.filter { m -> 365 >= diferencaDeDias(m) }.count()
        val crianca = membros.filter { m -> 365 * 18 >= diferencaDeDias(m) && diferencaDeDias(m) > 365 }.count()
        val jovem = membros.filter { m -> 365 * 29 >= diferencaDeDias(m) && diferencaDeDias(m) > 365 * 18 }.count()
        val adulto = membros.filter { m -> 365 * 59 >= diferencaDeDias(m) && diferencaDeDias(m) > 365 * 29 }.count()
        val idoso = membros.filter { m -> diferencaDeDias(m) > 365 * 59 }.count()

        val data: MutableMap<String, Any> = mutableMapOf()

        val dataSet: MutableList<MutableMap<String, Any>> = mutableListOf()
        dataSet.add(hashMapOf("label" to "Bebê", "data" to listOf(bebe), "backgroundColor" to "#8E24AA"))
        dataSet.add(hashMapOf("label" to "Criança", "data" to listOf(crianca), "backgroundColor" to "#1976D2"))
        dataSet.add(hashMapOf("label" to "Jovem", "data" to listOf(jovem), "backgroundColor" to "#26A69A"))
        dataSet.add(hashMapOf("label" to "Adulto", "data" to listOf(adulto), "backgroundColor" to "#E65100"))
        dataSet.add(hashMapOf("label" to "Idoso", "data" to listOf(idoso), "backgroundColor" to "#6200EA"))
        data["datasets"] = dataSet

        return data
    }

    private fun diferencaDeDias(m: Membro) =
            ChronoUnit.DAYS.between(m.dataNascimento, LocalDate.now())

    override fun membrosAtivosEInativos(igreja: Long?): Any {
        val membros = if (igreja != null) repository.findByIgrejaId(igreja) else repository.findAll()

        if (membros.isNullOrEmpty()) throw IllegalArgumentException()

        val totalI = membros.filter { m -> if (m.inativo != null) m.inativo!! else false }.count()
        val totalA = membros.filter { m -> if (m.inativo != null) !m.inativo!! else true }.count()

        val data: MutableMap<String, Any> = mutableMapOf()

        val dataSet: MutableList<MutableMap<String, Any>> = mutableListOf()
        dataSet.add(hashMapOf("label" to "Ativos", "data" to listOf(totalA), "backgroundColor" to "#0D47A1"))
        dataSet.add(hashMapOf("label" to "Inativos", "data" to listOf(totalI), "backgroundColor" to "#6200EA"))
        data["datasets"] = dataSet

        return data
    }
}
