package br.com.felix.tccserver.membro

import br.com.felix.tccserver.igreja.Igreja
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

@Entity
data class Membro(

        @Column(nullable = false, length = 100)
        var nome: String,

        @Column(nullable = false)
        var dataNascimento: LocalDate,

        @ManyToOne(optional = false)
        @JoinColumn
        var igreja: Igreja,

        @Enumerated
        @Column(nullable = false)
        var sexo: Sexo,

        var endereco: String? = null,

        var email: String? = null,

        var telefone: String? = null,

        var salario: BigDecimal? = null,

        var inativo: Boolean? = false,

        @ManyToMany
        var filhos: List<Membro> = emptyList(),

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
