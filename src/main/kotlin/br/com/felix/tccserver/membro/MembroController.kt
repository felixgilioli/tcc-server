package br.com.felix.tccserver.membro

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@RequestMapping("membro")
class MembroController(private val service: MembroService) : CrudController<Membro, Long>() {

    override fun getService() = service

    @GetMapping("aniversariantes")
    fun findAniversariantes(@RequestParam("data") dataAniversario: LocalDate) =
            service.findAniversariantes(dataAniversario)

    @GetMapping("igreja/{igreja}")
    fun findByIgreja(@PathVariable("igreja") igreja: Long) =
            service.findByIgreja(igreja)

    @GetMapping("complete")
    fun complete(@RequestParam("query") query: String) = service.complete(query)

    @GetMapping("membroporsexo")
    fun membrosPorSexo(@RequestParam("igreja", required = false) igreja: Long?) = service.membrosPorSexo(igreja)

    @GetMapping("membroporfaixaetaria")
    fun membrosPorFaixaEtaria(@RequestParam("igreja", required = false) igreja: Long?) = service.membrosPorFaixaEtaria(igreja)

    @GetMapping("ativoseinativos")
    fun membrosAtivosEInativos(@RequestParam("igreja", required = false) igreja: Long?) = service.membrosAtivosEInativos(igreja)
}
