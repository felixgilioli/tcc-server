package br.com.felix.tccserver.membro

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDate

interface MembroRepository : JpaRepository<Membro, Long> {

    fun findByDataNascimento(dataAniversario: LocalDate): List<Membro>?

    fun findByIgrejaId(igreja: Long): List<Membro>?

    fun findByNomeContainingIgnoreCase(nome: String): List<Membro>?

}
