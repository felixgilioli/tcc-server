package br.com.felix.tccserver.notify

import br.com.felix.tccserver.framework.email.EmailModel
import br.com.felix.tccserver.framework.email.EmailSender
import br.com.felix.tccserver.membro.MembroService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class MensagemDoDia(private val membroService: MembroService,
                    private val emailSender: EmailSender) {

    @Scheduled(cron = "0 48 19 * * *")
    fun enviaMensagem() {
        val emails = membroService.findAll()
                ?.mapNotNull { m -> m.email }
                ?.toSet()
        if (emails != null) {
            emailSender.send(EmailModel(
                    emails,
                    "Salmo do dia",
                    "Agora, assim diz o Senhor dos Exércitos: " +
                            "Vejam aonde os seus caminhos os levaram. "
            ))
        }
    }
}
