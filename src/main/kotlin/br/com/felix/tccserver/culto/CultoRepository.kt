package br.com.felix.tccserver.culto

import org.springframework.data.jpa.repository.JpaRepository

interface CultoRepository : JpaRepository<Culto, Long> {
}
