package br.com.felix.tccserver.culto.confirmacao

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("confirmacao")
class ConfirmacaoPresencaController(private val service: ConfirmacaoPresencaService)
    : CrudController<ConfirmacaoPresenca, Long>() {

    override fun getService() = service
}
