package br.com.felix.tccserver.culto.confirmacao

import br.com.felix.tccserver.culto.Culto
import br.com.felix.tccserver.membro.Membro
import javax.persistence.*

@Entity
data class ConfirmacaoPresenca(

        @ManyToOne
        @JoinColumn
        val culto: Culto,

        @ManyToOne
        @JoinColumn
        val membro: Membro,

        var confirmado: Boolean = false,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
