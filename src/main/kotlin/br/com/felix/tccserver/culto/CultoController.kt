package br.com.felix.tccserver.culto

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("culto")
class CultoController(private val service: CultoService) : CrudController<Culto, Long>() {

    override fun getService() = service
}
