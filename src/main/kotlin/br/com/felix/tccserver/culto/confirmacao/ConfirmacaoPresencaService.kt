package br.com.felix.tccserver.culto.confirmacao

import br.com.felix.tccserver.framework.crud.CrudService

interface ConfirmacaoPresencaService : CrudService<ConfirmacaoPresenca, Long> {
}
