package br.com.felix.tccserver.culto.confirmacao

import org.springframework.data.jpa.repository.JpaRepository

interface ConfirmacaoPresencaRepository : JpaRepository<ConfirmacaoPresenca, Long> {
}
