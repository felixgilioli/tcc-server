package br.com.felix.tccserver.culto

import br.com.felix.tccserver.framework.crud.CrudService

interface CultoService : CrudService<Culto, Long> {
}
