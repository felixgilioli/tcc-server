package br.com.felix.tccserver.culto

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class CultoServiceImpl(private val repository: CultoRepository) : CrudServiceImpl<Culto, Long>(), CultoService {

    override fun getRepository() = repository
}
