package br.com.felix.tccserver.culto

import br.com.felix.tccserver.igreja.Igreja
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Culto(

        @ManyToOne
        @JoinColumn
        val igreja: Igreja,

        var data: LocalDateTime,

        var titulo: String,

        var descricao: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
