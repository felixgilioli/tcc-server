package br.com.felix.tccserver.culto.confirmacao

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class ConfirmacaoPresencaServiceImpl(private val repository: ConfirmacaoPresencaRepository) : CrudServiceImpl<ConfirmacaoPresenca, Long>(), ConfirmacaoPresencaService {

    override fun getRepository() = repository
}
