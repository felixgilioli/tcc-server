package br.com.felix.tccserver.conta

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("conta")
class ContaBancariaController(private val service: ContaBancariaService) : CrudController<ContaBancaria, Long>() {

    override fun getService() = service
}
