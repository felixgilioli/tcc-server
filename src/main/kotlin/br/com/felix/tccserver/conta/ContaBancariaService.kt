package br.com.felix.tccserver.conta

import br.com.felix.tccserver.framework.crud.CrudService

interface ContaBancariaService : CrudService<ContaBancaria, Long> {
}
