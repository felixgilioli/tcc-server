package br.com.felix.tccserver.conta

import java.math.BigDecimal
import javax.persistence.*

@Entity
data class ContaBancaria(

        var nome: String,

        var titular: String,

        var cpfCnpj: String,

        var saldo: BigDecimal,

        val numero: String,

        var contaPadrao: Boolean = false,

        @Enumerated(EnumType.STRING)
        val tipoConta: TipoConta,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
