package br.com.felix.tccserver.conta

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class ContaBancariaServiceImpl(private val repository: ContaBancariaRepository) : CrudServiceImpl<ContaBancaria, Long>(), ContaBancariaService {

    override fun getRepository() = repository
}
