package br.com.felix.tccserver.conta

import org.springframework.data.jpa.repository.JpaRepository

interface ContaBancariaRepository : JpaRepository<ContaBancaria, Long> {
}
