package br.com.felix.tccserver.conta

enum class TipoConta {

    CONTA_CORRENTE,
    CONTA_POUPANCA
}
