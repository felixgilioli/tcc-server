package br.com.felix.tccserver.patrimonio.categoria

import org.springframework.data.jpa.repository.JpaRepository

interface CategoriaRepository : JpaRepository<Categoria, Long> {

    fun findByNomeContainingIgnoreCase(query: String): List<Categoria>?
}
