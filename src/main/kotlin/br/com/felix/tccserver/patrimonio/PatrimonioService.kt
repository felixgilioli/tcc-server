package br.com.felix.tccserver.patrimonio

import br.com.felix.tccserver.framework.crud.CrudService

interface PatrimonioService : CrudService<Patrimonio, Long> {

    fun findByIgreja(igreja: Long): List<Patrimonio>

    fun findByCategoria(categoria: Long, igreja: Long): List<Patrimonio>

    fun complete(query: String): List<Patrimonio>
}
