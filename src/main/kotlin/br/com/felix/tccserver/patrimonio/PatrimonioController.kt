package br.com.felix.tccserver.patrimonio

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("patrimonio")
class PatrimonioController(private val service: PatrimonioService) : CrudController<Patrimonio, Long>() {

    override fun getService() = service

    @GetMapping("igreja/{igreja}")
    fun findByIgreja(@PathVariable("igreja") igreja: Long) =
            service.findByIgreja(igreja)

    @GetMapping("igreja/{igreja}/categoria/{categoria}")
    fun findByCategoria(@PathVariable("igreja") igreja: Long,
                        @PathVariable("categoria") categoria: Long) =
            service.findByCategoria(categoria, igreja)

    @GetMapping("complete")
    fun complete(@RequestParam("query") query: String) = service.complete(query)
}
