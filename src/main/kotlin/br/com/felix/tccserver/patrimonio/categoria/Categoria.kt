package br.com.felix.tccserver.patrimonio.categoria

import javax.persistence.*

@Entity
data class Categoria(

        @Column(length = 100, nullable = false)
        var nome: String,

        @Column(length = 255)
        var descricao: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
