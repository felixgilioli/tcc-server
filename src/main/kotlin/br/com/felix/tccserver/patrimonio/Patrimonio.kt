package br.com.felix.tccserver.patrimonio

import br.com.felix.tccserver.igreja.Igreja
import br.com.felix.tccserver.patrimonio.categoria.Categoria
import javax.persistence.*

@Entity
data class Patrimonio(

        var nome: String,

        var quantidade: Int,

        @ManyToOne
        @JoinColumn
        var categoria: Categoria,

        @ManyToOne
        @JoinColumn
        var igreja: Igreja,

        var inativo: Boolean = false,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
