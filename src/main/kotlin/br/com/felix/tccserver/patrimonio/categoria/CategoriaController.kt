package br.com.felix.tccserver.patrimonio.categoria

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("categoria")
class CategoriaController(private val service: CategoriaService) : CrudController<Categoria, Long>() {

    override fun getService() = service

    @GetMapping("complete")
    fun complete(@RequestParam("query") query: String) =
            service.complete(query)

}
