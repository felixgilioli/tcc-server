package br.com.felix.tccserver.patrimonio.categoria

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CategoriaServiceImpl(private val repository: CategoriaRepository) : CategoriaService, CrudServiceImpl<Categoria, Long>() {

    override fun getRepository() = repository

    @Transactional(readOnly = true)
    override fun complete(query: String) =
            repository.findByNomeContainingIgnoreCase(query) ?: emptyList()

}
