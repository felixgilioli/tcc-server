package br.com.felix.tccserver.patrimonio.categoria

import br.com.felix.tccserver.framework.crud.CrudService

interface CategoriaService : CrudService<Categoria, Long> {

    fun complete(query: String): List<Categoria>
}
