package br.com.felix.tccserver.patrimonio

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class PatrimonioServiceImpl(private val repository: PatrimonioRepository) : PatrimonioService, CrudServiceImpl<Patrimonio, Long>() {

    override fun getRepository() = repository

    @Transactional(readOnly = true)
    override fun findByIgreja(igreja: Long) =
            repository.findByIgrejaId(igreja) ?: emptyList()

    @Transactional(readOnly = true)
    override fun findByCategoria(categoria: Long, igreja: Long) =
            repository.findByIgrejaIdAndCategoriaId(igreja, categoria) ?: emptyList()

    @Transactional(readOnly = true)
    override fun complete(query: String) = repository
            .findByNomeContainingIgnoreCase(query) ?: emptyList()
}
