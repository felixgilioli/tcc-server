package br.com.felix.tccserver.patrimonio

import org.springframework.data.jpa.repository.JpaRepository

interface PatrimonioRepository : JpaRepository<Patrimonio, Long> {

    fun findByIgrejaId(igreja: Long): List<Patrimonio>?

    fun findByIgrejaIdAndCategoriaId(igreja: Long, categoria: Long): List<Patrimonio>?

    fun findByNomeContainingIgnoreCase(query: String): List<Patrimonio>?
}
