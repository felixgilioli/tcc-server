package br.com.felix.tccserver.framework.validator

interface Validator<T> {

    @Throws(ValidatorException::class)
    fun valida(o: T)
}
