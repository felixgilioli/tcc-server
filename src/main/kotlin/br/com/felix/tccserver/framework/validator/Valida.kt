package br.com.felix.tccserver.framework.validator

@Throws(ValidatorException::class)
fun <T> valida(validator: Validator<T>, o: T) {
    validator.valida(o)
}
