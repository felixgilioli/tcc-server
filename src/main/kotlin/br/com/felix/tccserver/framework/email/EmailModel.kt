package br.com.felix.tccserver.framework.email

data class EmailModel(
        val para: Set<String>,
        val titulo: String,
        val mensagem: String
)
