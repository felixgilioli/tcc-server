package br.com.felix.tccserver.framework.email

class EmailException(message: String?) : Exception(message)
