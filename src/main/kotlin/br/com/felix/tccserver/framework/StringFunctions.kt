package br.com.felix.tccserver.framework

import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun String.getLocalDate(): LocalDate {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    return LocalDate.parse(this, formatter)
}
