package br.com.felix.tccserver.framework.validator

import java.lang.Exception

class ValidatorException(message: String?) : Exception(message)
