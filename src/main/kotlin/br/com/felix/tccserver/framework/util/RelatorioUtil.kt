package br.com.felix.tccserver.framework.util

import org.apache.pdfbox.pdmodel.font.PDType1Font
import rst.pdfbox.layout.elements.Document
import rst.pdfbox.layout.elements.Paragraph
import rst.pdfbox.layout.elements.render.ColumnLayout
import rst.pdfbox.layout.elements.render.VerticalLayoutHint
import rst.pdfbox.layout.text.Alignment
import java.io.ByteArrayOutputStream

val FONT = PDType1Font.HELVETICA
val FONT_BOLD = PDType1Font.HELVETICA_BOLD

class RelatorioUtil {

    data class Relatorio(
            val titulo: String,
            val colunas: List<String>,
            val valores: List<List<Any>>
    )

    fun gerarRelatorio(relatorio: Relatorio): ByteArray {
        val document = Document()

        val paragraph = Paragraph()
        paragraph.addText(relatorio.titulo, 20f, FONT)
        document.add(paragraph, VerticalLayoutHint(Alignment.Center, 0f, 0f, 20f, 20f))

        document.add(ColumnLayout(relatorio.colunas.size, 0f))

        relatorio.colunas.forEachIndexed { index, coluna ->
            val left = Paragraph()
            left.addText(coluna, 12f, FONT_BOLD)
            document.add(left, VerticalLayoutHint(Alignment.Center, 0f, 0f, 0f, 10f))

            if (index != relatorio.colunas.size -1) {
                document.add(ColumnLayout.NEWCOLUMN)
            }
        }


        val columns = relatorio.valores.size
        val rows = relatorio.valores.first().size


        (0 until rows).forEach { row ->

            document.add(ColumnLayout(columns, 0f))

            (0 until columns).forEach { column ->

                val coluna1 = Paragraph()
                coluna1.addText(relatorio.valores[column][row].toString(), 12f, FONT)
                document.add(coluna1, VerticalLayoutHint(Alignment.Center))

                if (column != columns -1) {
                    document.add(ColumnLayout.NEWCOLUMN)
                }
            }
        }

        val out = ByteArrayOutputStream()

        document.save(out)

        return out.toByteArray()
    }
}
