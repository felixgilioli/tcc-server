package br.com.felix.tccserver.framework.email

interface EmailSender {

    @Throws(EmailException::class)
    fun send(model: EmailModel)
}
