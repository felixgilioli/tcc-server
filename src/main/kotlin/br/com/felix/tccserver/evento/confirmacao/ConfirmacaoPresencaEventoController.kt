package br.com.felix.tccserver.evento.confirmacao

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("evento/confirmacao")
class ConfirmacaoPresencaEventoController(private val service: ConfirmacaoPresencaEventoService) :
        CrudController<ConfirmacaoPresencaEvento, Long>() {

    override fun getService() = service
}
