package br.com.felix.tccserver.evento

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.framework.email.EmailModel
import br.com.felix.tccserver.framework.email.EmailSender
import br.com.felix.tccserver.membro.Membro
import br.com.felix.tccserver.membro.MembroService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.springframework.stereotype.Service

@Service
class EventoServiceImpl(private val repository: EventoRepository,
                        private val emailSender: EmailSender,
                        private val membroService: MembroService) :
        CrudServiceImpl<Evento, Long>(), EventoService {

    override fun getRepository() = repository

    override fun promover(evento: Evento) {
        val emails = membroService
                .findAll()
                ?.mapNotNull(Membro::email)

        if (!emails.isNullOrEmpty()) {
            GlobalScope.launch {
                emailSender.send(EmailModel(
                        emails.toSet(),
                        "Novo evento",
                        "Dia ${evento.data} " +
                                "acontecerá o evento ${evento.titulo} " +
                                "no endereco: ${evento.localizacao}. <br><br> " +
                                "Contamos com sua presença."
                ))
            }
        }
    }
}
