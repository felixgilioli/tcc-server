package br.com.felix.tccserver.evento.confirmacao

import br.com.felix.tccserver.evento.Evento
import br.com.felix.tccserver.membro.Membro
import javax.persistence.*

@Entity
data class ConfirmacaoPresencaEvento (

    @ManyToOne
    @JoinColumn
    val evento: Evento,

    @ManyToOne
    @JoinColumn
    val membro: Membro,

    var confirmado: Boolean = false,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
)
