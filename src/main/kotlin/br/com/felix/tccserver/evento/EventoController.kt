package br.com.felix.tccserver.evento

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("evento")
class EventoController(private val service: EventoService) : CrudController<Evento, Long>() {

    override fun getService() = service

    @PostMapping("promover")
    fun promover(@RequestBody evento: Evento) = service.promover(evento)
}
