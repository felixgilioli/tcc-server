package br.com.felix.tccserver.evento.confirmacao

import br.com.felix.tccserver.framework.crud.CrudService

interface ConfirmacaoPresencaEventoService : CrudService<ConfirmacaoPresencaEvento, Long> {

}
