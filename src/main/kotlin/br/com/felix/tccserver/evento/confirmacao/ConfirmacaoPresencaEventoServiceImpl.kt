package br.com.felix.tccserver.evento.confirmacao

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class ConfirmacaoPresencaEventoServiceImpl(private val repository: ConfirmacaoPresencaEventoRepository) :
        CrudServiceImpl<ConfirmacaoPresencaEvento, Long>(), ConfirmacaoPresencaEventoService {

    override fun getRepository() = repository

}
