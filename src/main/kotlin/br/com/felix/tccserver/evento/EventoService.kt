package br.com.felix.tccserver.evento

import br.com.felix.tccserver.framework.crud.CrudService

interface EventoService : CrudService<Evento, Long> {

    fun promover(evento: Evento)
}
