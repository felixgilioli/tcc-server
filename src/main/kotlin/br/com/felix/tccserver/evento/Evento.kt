package br.com.felix.tccserver.evento

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Evento(

        var titulo: String,

        var descricao: String,

        var data: LocalDateTime,

        var localizacao: String,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
