package br.com.felix.tccserver.evento.confirmacao

import br.com.felix.tccserver.evento.Evento
import org.springframework.data.jpa.repository.JpaRepository

interface ConfirmacaoPresencaEventoRepository : JpaRepository<ConfirmacaoPresencaEvento, Long> {

    fun findByEvento(evento: Evento): List<ConfirmacaoPresencaEvento>?
}
