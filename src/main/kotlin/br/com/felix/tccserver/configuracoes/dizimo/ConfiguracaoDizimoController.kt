package br.com.felix.tccserver.configuracoes.dizimo

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("configdizimo")
class ConfiguracaoDizimoController(private val service: ConfiguracaoDizimoService) : CrudController<ConfiguracaoDizimo, Long>() {

    override fun getService() = service
}
