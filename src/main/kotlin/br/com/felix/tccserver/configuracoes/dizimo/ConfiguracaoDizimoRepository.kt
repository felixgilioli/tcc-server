package br.com.felix.tccserver.configuracoes.dizimo

import org.springframework.data.jpa.repository.JpaRepository

interface ConfiguracaoDizimoRepository : JpaRepository<ConfiguracaoDizimo, Long> {
}
