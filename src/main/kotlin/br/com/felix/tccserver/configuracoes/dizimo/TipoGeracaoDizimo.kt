package br.com.felix.tccserver.configuracoes.dizimo

enum class TipoGeracaoDizimo {

    GERAR_AUTOMATICAMENTE,
    PAGAMENTO_EXPONTANEO
}
