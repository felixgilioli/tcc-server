package br.com.felix.tccserver.configuracoes.dizimo

import br.com.felix.tccserver.framework.crud.CrudService

interface ConfiguracaoDizimoService : CrudService<ConfiguracaoDizimo, Long> {
}
