package br.com.felix.tccserver.configuracoes.dizimo

import javax.persistence.*

@Entity
data class ConfiguracaoDizimo(

        @Enumerated(EnumType.STRING)
        var tipoGeracaoDizimo: TipoGeracaoDizimo = TipoGeracaoDizimo.PAGAMENTO_EXPONTANEO,

        var diaGeracaoMes: Int? = null,

        var porcentagemSalario: Double? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
