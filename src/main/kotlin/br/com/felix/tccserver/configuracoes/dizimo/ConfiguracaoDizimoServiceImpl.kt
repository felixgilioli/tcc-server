package br.com.felix.tccserver.configuracoes.dizimo

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class ConfiguracaoDizimoServiceImpl(private val repository: ConfiguracaoDizimoRepository) : ConfiguracaoDizimoService, CrudServiceImpl<ConfiguracaoDizimo, Long>() {

    override fun getRepository() = repository
}
