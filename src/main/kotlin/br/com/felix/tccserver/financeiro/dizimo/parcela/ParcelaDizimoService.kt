package br.com.felix.tccserver.financeiro.dizimo.parcela

import br.com.felix.tccserver.framework.crud.CrudService

interface ParcelaDizimoService : CrudService<ParcelaDizimo, Long> {
}
