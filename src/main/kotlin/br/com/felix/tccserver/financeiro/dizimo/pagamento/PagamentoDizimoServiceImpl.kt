package br.com.felix.tccserver.financeiro.dizimo.pagamento

import br.com.felix.tccserver.financeiro.dizimo.pagamento.listener.PagouDizimoListener
import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.framework.util.FONT
import br.com.felix.tccserver.framework.util.RelatorioUtil
import br.com.felix.tccserver.membro.Membro
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.springframework.stereotype.Service
import rst.pdfbox.layout.elements.Document
import rst.pdfbox.layout.elements.Paragraph
import rst.pdfbox.layout.elements.render.VerticalLayoutHint
import rst.pdfbox.layout.text.Alignment
import java.io.ByteArrayOutputStream
import java.math.BigDecimal
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@Service
class PagamentoDizimoServiceImpl(private val repository: PagamentoDizimoRepository,
                                 private val pagouDizimoListener: Set<PagouDizimoListener>) : PagamentoDizimoService, CrudServiceImpl<PagamentoDizimo, Long>() {

    override fun getRepository() = repository

    override fun postSave(entity: PagamentoDizimo): PagamentoDizimo {
        pagouDizimoListener
                .stream()
                .forEach { p ->
                    run {
                        GlobalScope.launch {
                            p.executa(entity)
                        }
                    }
                }

        return super.postSave(entity)
    }

    override fun contribuicaoPorFiel(dtInicio: LocalDate, dtFim: LocalDate): ByteArray {
        val contribuicoes = repository.findByDataBetween(dtInicio.atStartOfDay(), dtFim.atStartOfDay())

        if (contribuicoes.isNullOrEmpty()) throw RuntimeException("Não foi possível obter dados.")

        val map = contribuicoes.groupBy(PagamentoDizimo::membro)

        val lista = listOf(
                map.keys.map(Membro::nome),
                map.values.map { v -> "R$ ${v.map(PagamentoDizimo::valor).reduce(BigDecimal::add)}" }
        )

        return RelatorioUtil().gerarRelatorio(RelatorioUtil.Relatorio(
                "Relatório de Contribuição por Fiel",
                listOf("Membro", "Contribuição Total"),
                lista
        ))

    }

    override fun imprimirComprovante(pagamentoId: Long): ByteArray {
        val pagamento = repository.findById(pagamentoId)
        if (!pagamento.isPresent) throw IllegalArgumentException("pagamento inválido.")

        val pagamentoDizimo = pagamento.get()

        val document = Document()

        val paragraph = Paragraph()

        val data = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(pagamentoDizimo.data)

        paragraph.addText("Membro ${pagamentoDizimo.membro.nome} " +
                "pagou o dizimo no valor de R$ ${pagamentoDizimo.valor} " +
                "no dia $data.", 16f, FONT)

        document.add(paragraph, VerticalLayoutHint(Alignment.Justify, 20f, 0f, 20f, 20f))

        val out = ByteArrayOutputStream()

        document.save(out)

        return out.toByteArray()
    }
}
