package br.com.felix.tccserver.financeiro.dizimo.parcela

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("parceladizimo")
class ParcelaDizimoController(private val service: ParcelaDizimoService) : CrudController<ParcelaDizimo, Long>() {

    override fun getService() = service
}
