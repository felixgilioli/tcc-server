package br.com.felix.tccserver.financeiro.despesa

import org.springframework.data.jpa.repository.JpaRepository

interface DespesaRepository : JpaRepository<Despesa, Long>
