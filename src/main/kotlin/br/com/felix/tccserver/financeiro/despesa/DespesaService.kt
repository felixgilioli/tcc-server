package br.com.felix.tccserver.financeiro.despesa

import br.com.felix.tccserver.framework.crud.CrudService

interface DespesaService : CrudService<Despesa, Long> {
}
