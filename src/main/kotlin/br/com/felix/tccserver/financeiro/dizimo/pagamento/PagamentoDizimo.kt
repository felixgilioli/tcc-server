package br.com.felix.tccserver.financeiro.dizimo.pagamento

import br.com.felix.tccserver.conta.ContaBancaria
import br.com.felix.tccserver.membro.Membro
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class PagamentoDizimo(

        val data: LocalDateTime,

        @ManyToOne
        @JoinColumn
        val membro: Membro,

        val valor: BigDecimal,

        val observacao: String? = null,

        @ManyToOne
        @JoinColumn
        val conta: ContaBancaria? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
