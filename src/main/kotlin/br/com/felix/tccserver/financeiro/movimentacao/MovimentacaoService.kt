package br.com.felix.tccserver.financeiro.movimentacao

import br.com.felix.tccserver.framework.crud.CrudService
import java.math.BigDecimal
import java.time.LocalDate
import java.time.YearMonth

interface MovimentacaoService : CrudService<Movimentacao, Long> {

    fun findByIgreja(igreja: Long): List<Movimentacao>

    fun lucroPorMes(mes: YearMonth): BigDecimal

    fun receitasPorDespesa(dataInicio: LocalDate, dataFim: LocalDate): Any
}
