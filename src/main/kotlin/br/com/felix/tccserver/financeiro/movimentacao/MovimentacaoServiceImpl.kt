package br.com.felix.tccserver.financeiro.movimentacao

import br.com.felix.tccserver.conta.ContaBancariaService
import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.RuntimeException
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.YearMonth

@Service
class MovimentacaoServiceImpl(private val repository: MovimentacaoRepository,
                              private val contaBancariaService: ContaBancariaService) : MovimentacaoService, CrudServiceImpl<Movimentacao, Long>() {

    override fun getRepository() = repository

    @Transactional(readOnly = true)
    override fun findByIgreja(igreja: Long) =
            repository.findByIgrejaId(igreja) ?: emptyList()

    override fun postSave(entity: Movimentacao): Movimentacao {
        if (entity.conta != null) {
            entity.conta.saldo = entity.conta.saldo.add(entity.valor)
            contaBancariaService.save(entity.conta)
        }
        return super.postSave(entity)
    }

    override fun receitasPorDespesa(dataInicio: LocalDate, dataFim: LocalDate): Any {
        val movimentacoes = repository.findByDataBetween(dataInicio.atStartOfDay(), dataFim.atStartOfDay())

        if (movimentacoes.isNullOrEmpty()) throw RuntimeException("Não foi possível obter os dados")

        val meses : MutableList<YearMonth> = mutableListOf()
        val despesas : MutableList<BigDecimal> = mutableListOf()
        val receitas : MutableList<BigDecimal> = mutableListOf()

        movimentacoes.onEach { m -> m.mes = YearMonth.from(m.data) }
                .groupBy(Movimentacao::mes)
                .forEach { key, value ->
                    run {
                        meses.add(key!!)
                        despesas.add(getValorTotal(TipoMovimentacao.SAIDA, value).negate())
                        receitas.add(getValorTotal(TipoMovimentacao.ENTRADA, value))
                    }
                }
        val data : MutableMap<String, Any> = mutableMapOf()
        data["labels"] = meses

        val dataSet : MutableList<MutableMap<String, Any>> = mutableListOf()
        dataSet.add(hashMapOf("label" to "Receitas", "data" to receitas, "fill" to false, "borderColor" to "#007ad9"))
        dataSet.add(hashMapOf("label" to "Despesas", "data" to despesas, "fill" to false, "borderColor" to "#dc3545"))
        data["datasets"] = dataSet

        return data
    }

    private fun getValorTotal(tipo: TipoMovimentacao, value: List<Movimentacao>): BigDecimal {
        return value
                .filter { m -> m.tipo == tipo }
                .map(Movimentacao::valor)
                .fold(BigDecimal.ZERO) { a, b -> a.add(b) }
    }

    override fun lucroPorMes(mes: YearMonth): BigDecimal {
        val movimentacoesDoMes = repository.findByDataBetween(
                LocalDateTime.of(mes.year, mes.month, 1, 0, 0),
                LocalDateTime.of(mes.year, mes.month, mes.atEndOfMonth().dayOfMonth, 0, 0))

        if (movimentacoesDoMes.isNullOrEmpty())
            return BigDecimal.ZERO

        val totalEntrada = filterMovimentacoesByTipo(movimentacoesDoMes, TipoMovimentacao.ENTRADA)
        val totalSaida = filterMovimentacoesByTipo(movimentacoesDoMes, TipoMovimentacao.SAIDA)

        return totalEntrada.subtract(totalSaida)
    }

    private fun filterMovimentacoesByTipo(movimentacoes: List<Movimentacao>, tipo: TipoMovimentacao) =
            movimentacoes
                    .filter { it.tipo == tipo }
                    .map { it.valor }
                    .reduce(BigDecimal::add)

}
