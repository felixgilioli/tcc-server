package br.com.felix.tccserver.financeiro.compras

import br.com.felix.tccserver.conta.ContaBancaria
import br.com.felix.tccserver.igreja.Igreja
import br.com.felix.tccserver.patrimonio.Patrimonio
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Compra(

        @ManyToOne
        @JoinColumn
        val igreja: Igreja,

        val data: LocalDateTime,

        var valor: BigDecimal,

        @ManyToOne
        @JoinColumn
        val patrimonio: Patrimonio,

        val quantidade: Int,

        @ManyToOne
        @JoinColumn
        val conta: ContaBancaria? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
