package br.com.felix.tccserver.financeiro.dizimo.parcela

import br.com.felix.tccserver.configuracoes.dizimo.ConfiguracaoDizimoService
import br.com.felix.tccserver.configuracoes.dizimo.TipoGeracaoDizimo
import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.membro.MembroService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class ParcelaDizimoServiceImpl(private val repository: ParcelaDizimoRepository,
                               private val configuracaoDizimoService: ConfiguracaoDizimoService,
                               private val membroService: MembroService) :
        ParcelaDizimoService, CrudServiceImpl<ParcelaDizimo, Long>() {

    override fun getRepository() = repository

    @Scheduled(cron = "0 0/50 * * * ?")
    fun gerarParcelas() {
        val configuracao = configuracaoDizimoService.findAll()?.get(0) ?: return

        if (configuracao.tipoGeracaoDizimo == TipoGeracaoDizimo.GERAR_AUTOMATICAMENTE
                && configuracao.diaGeracaoMes == LocalDate.now().dayOfMonth) {

            val parcelas = membroService.findAll()
                    ?.map { membro -> ParcelaDizimo.fromMembro(membro, configuracao) } ?: return

            repository.saveAll(parcelas)

        }
    }
}
