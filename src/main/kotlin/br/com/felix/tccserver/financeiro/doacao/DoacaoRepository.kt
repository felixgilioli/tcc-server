package br.com.felix.tccserver.financeiro.doacao

import org.springframework.data.jpa.repository.JpaRepository

interface DoacaoRepository : JpaRepository<Doacao, Long> {
}
