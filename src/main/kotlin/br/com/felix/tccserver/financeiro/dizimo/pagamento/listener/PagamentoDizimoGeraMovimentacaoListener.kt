package br.com.felix.tccserver.financeiro.dizimo.pagamento.listener

import br.com.felix.tccserver.financeiro.dizimo.pagamento.PagamentoDizimo
import br.com.felix.tccserver.financeiro.movimentacao.Movimentacao
import br.com.felix.tccserver.financeiro.movimentacao.MovimentacaoService
import br.com.felix.tccserver.financeiro.movimentacao.TipoMovimentacao
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class PagamentoDizimoGeraMovimentacaoListener(private val movimentacaoService: MovimentacaoService) : PagouDizimoListener {

    val logger = LoggerFactory.getLogger(PagamentoDizimoGeraMovimentacaoListener::class.java)!!

    override fun executa(pagamentoDizimo: PagamentoDizimo) {

        var movimentacao = Movimentacao(TipoMovimentacao.ENTRADA,
                pagamentoDizimo.valor,
                pagamentoDizimo.data,
                pagamentoDizimo.membro.igreja,
                pagamentoDizimo.conta)

        movimentacao = movimentacaoService.save(movimentacao)

        logger.info("Movimentação gerada: ${movimentacao.id} \n" +
                "pagamentoDizimo: ${pagamentoDizimo.id} \n" +
                "membro: ${pagamentoDizimo.membro.nome} \n" +
                "valor: ${pagamentoDizimo.valor} \n")
    }
}
