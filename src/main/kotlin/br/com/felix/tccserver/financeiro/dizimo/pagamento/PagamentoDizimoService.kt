package br.com.felix.tccserver.financeiro.dizimo.pagamento

import br.com.felix.tccserver.framework.crud.CrudService
import java.time.LocalDate

interface PagamentoDizimoService : CrudService<PagamentoDizimo, Long> {

    fun contribuicaoPorFiel(dtInicio: LocalDate, dtFim: LocalDate): ByteArray

    fun imprimirComprovante(pagamentoId: Long): ByteArray
}
