package br.com.felix.tccserver.financeiro.dizimo.parcela

import org.springframework.data.jpa.repository.JpaRepository

interface ParcelaDizimoRepository : JpaRepository<ParcelaDizimo, Long> {
}
