package br.com.felix.tccserver.financeiro.movimentacao

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDateTime

interface MovimentacaoRepository : JpaRepository<Movimentacao, Long> {

    fun findByIgrejaId(igreja: Long): List<Movimentacao>?

    fun findByDataBetween(primeiroDiaMes: LocalDateTime,
                          ultimoDiaMes: LocalDateTime): List<Movimentacao>?
}
