package br.com.felix.tccserver.financeiro.movimentacao

import br.com.felix.tccserver.framework.crud.CrudController
import br.com.felix.tccserver.framework.getLocalDate
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.time.YearMonth


@RestController
@RequestMapping("movimentacao")
class MovimentacaoController(private val service: MovimentacaoService) : CrudController<Movimentacao, Long>() {

    override fun getService() = service

    @GetMapping("igreja/{igreja}")
    fun findByIgreja(@PathVariable("igreja") igreja: Long) =
            service.findByIgreja(igreja)

    @GetMapping("ano/{ano}/mes/{mes}/lucro")
    fun lucroPorMes(@PathVariable("ano") ano: Int, @PathVariable("mes") mes: Int) =
            service.lucroPorMes(YearMonth.of(ano, mes))

    @GetMapping("receitapordespesa")
    fun receitasPorDespesa(@RequestParam("inicio") dataInicio: String?,
                    @RequestParam("fim") dataFim: String?): Any {
        val dtInicio = if (dataInicio.isNullOrBlank()) LocalDate.of(0, 1, 1) else dataInicio.getLocalDate()
        val dtFim = if (dataFim.isNullOrBlank()) LocalDate.of(9999, 1, 1) else dataFim.getLocalDate()

        return service.receitasPorDespesa(dtInicio, dtFim)
    }

}
