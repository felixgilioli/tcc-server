package br.com.felix.tccserver.financeiro.doacao

import br.com.felix.tccserver.conta.ContaBancaria
import br.com.felix.tccserver.igreja.Igreja
import br.com.felix.tccserver.membro.Membro
import br.com.felix.tccserver.patrimonio.Patrimonio
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class Doacao(

        @ManyToOne
        @JoinColumn
        val membro: Membro,

        @ManyToOne
        @JoinColumn
        val igreja: Igreja,

        val data: LocalDateTime,

        val valor: BigDecimal? = null,

        @ManyToOne
        @JoinColumn
        val patrimonio: Patrimonio? = null,

        val qtdePatrimonio: Int? = null,

        @ManyToOne
        @JoinColumn
        val conta: ContaBancaria? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
)
