package br.com.felix.tccserver.financeiro.dizimo.pagamento

import br.com.felix.tccserver.framework.crud.CrudController
import br.com.felix.tccserver.framework.getLocalDate
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@RequestMapping("dizimo/pagamento")
class PagamentoDizimoController(private val service: PagamentoDizimoService) : CrudController<PagamentoDizimo, Long>() {

    override fun getService() = service

    @GetMapping("contribuicaoporfiel", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun contribuicaoPorFiel(@RequestParam("inicio") dataInicio: String?,
                            @RequestParam("fim") dataFim: String?) : ByteArray {

        val dtInicio = if (dataInicio.isNullOrBlank()) LocalDate.of(0, 1, 1) else dataInicio.getLocalDate()
        val dtFim = if (dataFim.isNullOrBlank()) LocalDate.of(9999, 1, 1) else dataFim.getLocalDate()
        return service.contribuicaoPorFiel(dtInicio, dtFim)
    }

    @GetMapping("{pagamentoId}/imprimircomprovante")
    fun imprimirComprovante(@PathVariable pagamentoId: Long) : ByteArray {
        return service.imprimirComprovante(pagamentoId)
    }
}
