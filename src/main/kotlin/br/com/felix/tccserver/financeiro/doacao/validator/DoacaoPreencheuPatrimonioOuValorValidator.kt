package br.com.felix.tccserver.financeiro.doacao.validator

import br.com.felix.tccserver.financeiro.doacao.Doacao
import br.com.felix.tccserver.framework.validator.Validator
import br.com.felix.tccserver.framework.validator.ValidatorException

class DoacaoPreencheuPatrimonioOuValorValidator : Validator<Doacao> {

    override fun valida(o: Doacao) {
        if (o.valor == null && o.patrimonio == null)
            throw ValidatorException("valor ou patrimonio devem ser informados.")

        if (o.patrimonio != null && (o.qtdePatrimonio == null || o.qtdePatrimonio <= 0))
            throw ValidatorException("quantidade de patrimonio invalida.")
    }
}
