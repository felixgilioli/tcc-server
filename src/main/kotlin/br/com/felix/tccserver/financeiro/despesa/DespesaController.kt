package br.com.felix.tccserver.financeiro.despesa

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("despesa")
class DespesaController(private val despesaService: DespesaService) : CrudController<Despesa, Long>() {

    override fun getService() = despesaService
}
