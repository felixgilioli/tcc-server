package br.com.felix.tccserver.financeiro.movimentacao

enum class TipoMovimentacao {
    ENTRADA,
    SAIDA,
}
