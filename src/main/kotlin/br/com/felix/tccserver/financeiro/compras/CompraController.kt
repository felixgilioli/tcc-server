package br.com.felix.tccserver.financeiro.compras

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("compras")
class CompraController(private val service: CompraService) : CrudController<Compra, Long>() {

    override fun getService() = service
}
