package br.com.felix.tccserver.financeiro.dizimo.pagamento.listener

import br.com.felix.tccserver.financeiro.dizimo.pagamento.PagamentoDizimo

interface PagouDizimoListener {

    fun executa(pagamentoDizimo: PagamentoDizimo)
}
