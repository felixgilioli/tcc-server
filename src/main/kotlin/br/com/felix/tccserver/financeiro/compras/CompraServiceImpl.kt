package br.com.felix.tccserver.financeiro.compras

import br.com.felix.tccserver.financeiro.movimentacao.Movimentacao
import br.com.felix.tccserver.financeiro.movimentacao.MovimentacaoService
import br.com.felix.tccserver.financeiro.movimentacao.TipoMovimentacao
import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.patrimonio.PatrimonioService
import org.springframework.stereotype.Service

@Service
class CompraServiceImpl(private val repository: CompraRepository,
                        private val movimentacaoService: MovimentacaoService,
                        private val patrimonioService: PatrimonioService) : CrudServiceImpl<Compra, Long>(), CompraService {

    override fun getRepository() = repository

    override fun postSave(entity: Compra): Compra {
        movimentacaoService.save(Movimentacao(TipoMovimentacao.SAIDA,
                entity.valor.negate(),
                entity.data,
                entity.igreja,
                entity.conta))

        entity.patrimonio.quantidade += entity.quantidade

        patrimonioService.save(entity.patrimonio)

        return super.postSave(entity)
    }
}
