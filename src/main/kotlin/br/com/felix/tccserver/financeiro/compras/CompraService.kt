package br.com.felix.tccserver.financeiro.compras

import br.com.felix.tccserver.framework.crud.CrudService

interface CompraService : CrudService<Compra, Long> {
}
