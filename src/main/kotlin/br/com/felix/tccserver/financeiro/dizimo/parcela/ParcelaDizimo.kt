package br.com.felix.tccserver.financeiro.dizimo.parcela

import br.com.felix.tccserver.configuracoes.dizimo.ConfiguracaoDizimo
import br.com.felix.tccserver.membro.Membro
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class ParcelaDizimo(

        @ManyToOne
        @JoinColumn
        val membro: Membro,

        val valor: BigDecimal,

        var pago: Boolean = false,

        val dataVencimento: LocalDateTime,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null
) {
        companion object {
                fun fromMembro(membro: Membro, configuracaoDizimo: ConfiguracaoDizimo) : ParcelaDizimo =
                        ParcelaDizimo(membro,
                                membro.salario?.divide(BigDecimal(100))!!.multiply(BigDecimal(configuracaoDizimo.porcentagemSalario!!)),
                                false,
                                LocalDateTime.now().plusDays(10))
        }
}
