package br.com.felix.tccserver.financeiro.movimentacao

import br.com.felix.tccserver.conta.ContaBancaria
import br.com.felix.tccserver.igreja.Igreja
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.YearMonth
import javax.persistence.*

@Entity
data class Movimentacao(

        @Enumerated(EnumType.STRING)
        val tipo: TipoMovimentacao,

        val valor: BigDecimal,

        val data: LocalDateTime = LocalDateTime.now(),

        @ManyToOne
        @JoinColumn
        val igreja: Igreja,

        @ManyToOne
        @JoinColumn
        val conta: ContaBancaria? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @Transient
        var mes: YearMonth? = null
)
