package br.com.felix.tccserver.financeiro.doacao

import br.com.felix.tccserver.framework.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("doacao")
class DoacaoController(private val service: DoacaoService) : CrudController<Doacao, Long>() {

    override fun getService() = service

}
