package br.com.felix.tccserver.financeiro.dizimo.pagamento.listener

import br.com.felix.tccserver.financeiro.dizimo.pagamento.PagamentoDizimo
import br.com.felix.tccserver.framework.email.EmailModel
import br.com.felix.tccserver.framework.email.EmailSender
import org.springframework.stereotype.Service

@Service
class PagamentoDizimoNotificaMembro(private val emailSender: EmailSender) : PagouDizimoListener {

    override fun executa(pagamentoDizimo: PagamentoDizimo) {

        if (!pagamentoDizimo.membro.email.isNullOrBlank()) {
            val email = EmailModel(
                    setOf(pagamentoDizimo.membro.email!!),
                    "Pagamento de dizimo efetuado com sucesso",
                    "O pagamento de R$ ${pagamentoDizimo.valor} " +
                            "do seu dizimo foi realizado com sucesso."
            )

            emailSender.send(email)
        }
    }
}
