package br.com.felix.tccserver.financeiro.despesa

import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.financeiro.movimentacao.Movimentacao
import br.com.felix.tccserver.financeiro.movimentacao.MovimentacaoService
import br.com.felix.tccserver.financeiro.movimentacao.TipoMovimentacao
import org.springframework.stereotype.Service

@Service
class DespesaServiceImpl(private val repository: DespesaRepository,
                         private val movimentacaoService: MovimentacaoService) : CrudServiceImpl<Despesa, Long>(), DespesaService {

    override fun getRepository() = repository

    override fun postSave(entity: Despesa): Despesa {

        movimentacaoService.save(Movimentacao(TipoMovimentacao.SAIDA,
                entity.valor.negate(),
                entity.data,
                entity.igreja,
                entity.conta))

        return super.preSave(entity)
    }
}
