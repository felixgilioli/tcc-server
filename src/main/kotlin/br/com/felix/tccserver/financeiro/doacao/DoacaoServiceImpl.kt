package br.com.felix.tccserver.financeiro.doacao

import br.com.felix.tccserver.financeiro.doacao.validator.DoacaoPreencheuPatrimonioOuValorValidator
import br.com.felix.tccserver.framework.crud.CrudServiceImpl
import br.com.felix.tccserver.framework.validator.valida
import br.com.felix.tccserver.financeiro.movimentacao.Movimentacao
import br.com.felix.tccserver.financeiro.movimentacao.MovimentacaoService
import br.com.felix.tccserver.financeiro.movimentacao.TipoMovimentacao
import br.com.felix.tccserver.patrimonio.PatrimonioService
import org.springframework.stereotype.Service

@Service
class DoacaoServiceImpl(private val repository: DoacaoRepository,
                        private val movimentacaoService: MovimentacaoService,
                        private val patrimonioService: PatrimonioService) : DoacaoService, CrudServiceImpl<Doacao, Long>() {

    override fun getRepository() = repository

    override fun preSave(entity: Doacao): Doacao {
        valida(DoacaoPreencheuPatrimonioOuValorValidator(), entity)
        return entity
    }

    override fun postSave(entity: Doacao): Doacao {
        atualizaDados(entity)
        return super.postSave(entity)
    }

    private fun atualizaDados(entity: Doacao) {
        if (entity.valor != null) {
            val movimentacao = Movimentacao(TipoMovimentacao.ENTRADA,
                    entity.valor,
                    entity.data,
                    entity.igreja,
                    entity.conta)

            movimentacaoService.save(movimentacao)
        } else if (entity.patrimonio != null) {
            entity.patrimonio.quantidade += entity.qtdePatrimonio!!
            patrimonioService.save(entity.patrimonio)
        }
    }
}
