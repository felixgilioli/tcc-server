package br.com.felix.tccserver.financeiro.dizimo.pagamento

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDateTime

interface PagamentoDizimoRepository : JpaRepository<PagamentoDizimo, Long> {

    fun findByDataBetween(inicio: LocalDateTime?, fim: LocalDateTime?): List<PagamentoDizimo>?
}
