package br.com.felix.tccserver.financeiro.compras

import org.springframework.data.jpa.repository.JpaRepository

interface CompraRepository : JpaRepository<Compra, Long> {
}
