package br.com.felix.tccserver.financeiro.doacao

import br.com.felix.tccserver.framework.crud.CrudService

interface DoacaoService : CrudService<Doacao, Long> {
}
